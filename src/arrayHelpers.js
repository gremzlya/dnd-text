import flatten from 'lodash/flatten';

export const insertEntry = (content, textIndex, positionInText, entry) => {
  const newContent = [...content];
  let text = newContent[textIndex];

  text = [
    text.slice(0, positionInText),
    entry || { uid: textIndex },
    text.slice(positionInText)
  ];

  newContent[textIndex] = text;
    
  return flatten(newContent)
};

export const deleteEntry = (content, entryIndex) => {
  const newContent = [...content];

  if(typeof newContent[entryIndex - 1] === 'string' && typeof newContent[entryIndex + 1] === 'string') {
    newContent.splice(entryIndex - 1, 3, newContent[entryIndex -1] + ' ' + newContent[entryIndex + 1]);
  } else {
    newContent.splice(entryIndex, 1);
  }

  return newContent;
}

export const moveEntryTo = (content, entryIndex, textIndex, positionInText) => {
  const entry = content[entryIndex];
  const clearedContent = insertEntry(content, textIndex, positionInText, entry);

  let deleteEntryIndex = entryIndex;

  if (textIndex < entryIndex) {
    deleteEntryIndex = entryIndex + 2;
  }
  
  return deleteEntry(clearedContent, deleteEntryIndex)
};
