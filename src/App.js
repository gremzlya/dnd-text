import React, { Component } from 'react';
import select from 'selection-range';

import HTML5Backend from 'react-dnd-html5-backend';
import {
  DragDropContext,
} from 'react-dnd';

import FlipMove from 'react-flip-move';

import './App.css';
import DraggableDropableEntry from './Entry';
import * as arrayHelpers from './arrayHelpers'

class App extends Component {
  constructor() {
    super();

    this.updateEntry = this.updateEntry.bind(this);
    this.onBlur = this.onBlur.bind(this);
    this.addEntry = this.addEntry.bind(this);
    this.onDeleteEntry = this.onDeleteEntry.bind(this);
    this.moveEntry = this.moveEntry.bind(this);
  }

  state = {
    content: ['hello it\'s me', { uid: 1 }, 'realy \n I have', { uid: 2 }, 'been looking' ],
    cursorPosition: {
      entryIndex: null,
      positionIndex: null,
    },
    isDragging: false,
    dragableEntryIndex: null,
  }
  
  componentDidUpdate(prevProps, prevState) {
    if(this.state.content.length > prevState.content.length) {
      select(this.refs[`textEntry${this.state.cursorPosition.entryIndex + 2}`], { start: 0 })
    }
  }

  updateEntry(entryIndex, value, positionIndex) {
    const content = [...this.state.content];

    content[entryIndex] = value;

    this.setState({
      content,
      cursorPosition: {
        entryIndex,
        positionIndex,
      }
    });
  }

  onBlur(e, entryIndex) {
    const selection = select(e.target);
    const caretPosition = selection ? selection.end : null
    this.updateEntry(entryIndex, e.target.innerText, caretPosition)
  }

  moveEntry(entryIndex, textIndex, textPosition) {
    this.setState({
      content: arrayHelpers.moveEntryTo(this.state.content, entryIndex, textIndex, textPosition)
    });
  }

  addEntry() {
    const {
      content,
      cursorPosition
    } = this.state;

    this.setState({
      content: arrayHelpers.insertEntry(
        content,
        cursorPosition.entryIndex,
        cursorPosition.positionIndex
      ),
    })
  }

  onDeleteEntry(entryIndex) {
    this.setState({
      content: arrayHelpers.deleteEntry(this.state.content, entryIndex),
    });
  }

  renderText(text, tIndex)  {
    let length = 0;
    let wordsArray = [''];

    if (text !== '') {
      wordsArray = text.match(/(\s?\w+|\n)/g);
      console.log(wordsArray)
    }
    return (
      <span
        key={`text_${text}`}
        contentEditable
        onBlur={(e) => this.onBlur(e, tIndex)}
        ref={`textEntry${tIndex}`}
      >
      {
        wordsArray.map((textPart, index) => {
          length = length + textPart.length;

          if (textPart === '\n') {
            return  <br/>;
          } else {
            const textPartBeginIndex = length - textPart.length;

            return (
              <span onDragEnter={() => {
                this.moveEntry(this.state.dragableEntryIndex, tIndex, textPartBeginIndex);
              }}
              >
                {textPart}
              </span>
            )
          }
        })
      }
      </span>
    );
  }

  renderEntry(entry, tIndex) {
    return <div
      key={`uid_${entry.uid}`}
      style={{ border: '1px solid black', marginLeft: 10, marginRight: 10}}
      contentEditable={false}
      className="entry-title"
      onClick={() => this.onDeleteEntry(tIndex)}
    >
      <DraggableDropableEntry 
        uid={entry.uid}
        entryIndex={tIndex}
        onStartDragging={() => this.setState({ isDragging: true, dragableEntryIndex: tIndex })}
      />
    </div>
  }

  render() {
    return (
      <div  className="App">
        <div>
          {
            this.state.content.map(
              (text, tIndex) => (
                typeof text === 'string' ? this.renderText(text, tIndex) : this.renderEntry(text, tIndex)
              )
            )
          }
        </div>
        <button onClick={this.addEntry}>INSERT ENTRY</button>
      </div>
    );
  }
}

export default DragDropContext(HTML5Backend)(App);
