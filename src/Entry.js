
import React from 'react';
import {
  DragSource,
} from 'react-dnd';

const Entry = (props) => 
  props.connectDragSource(
    <span>uid = { props.uid }</span>
  )


const dragWrapper = DragSource(
  'test',
  {
    beginDrag(props) { props.onStartDragging(); return props; }
  },
  (connect, monitor) => ({
    connectDragSource: connect.dragSource(),
    isDragging: monitor.isDragging()
  }),
);

const WrappedEntry = 
  dragWrapper(
    Entry
  )

export default WrappedEntry;